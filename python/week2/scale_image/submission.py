import cv2

windowName = "Resize Image"
scaleTrackbar = "Scale"
typeTrackbar = "Type: \n 0: Scale Up \n 1: Scale Down"

class State:
    def __init__(self, scaleValue, scaleType):
        self.scaleValue = scaleValue
        self.scaleType = scaleType

image = cv2.imread("truth.png")

cv2.namedWindow(windowName, cv2.WINDOW_AUTOSIZE)

def scaleImage(state):
    if state.scaleType == 0:
        scaleFactor = 1 + state.scaleValue/100.0
    else:
        scaleFactor = max(0.01, 1 - state.scaleValue/100.0)

    scaledImage = cv2.resize(
            image,
            None,
            fx = scaleFactor,
            fy = scaleFactor,
            interpolation = cv2.INTER_LINEAR)
    cv2.imshow(windowName, scaledImage)

def updateScaleValue(scaleValue):
    global state
    state.scaleValue = scaleValue
    scaleImage(state)

def updateScaleType(scaleType):
    global state
    state.scaleType = scaleType
    scaleImage(state)

state = State(scaleValue = 25, scaleType = 0) 

# Create trackbars
cv2.createTrackbar(scaleTrackbar, windowName, 0, 100, updateScaleValue)
cv2.createTrackbar(typeTrackbar, windowName, 0, 1, updateScaleType)
cv2.setTrackbarPos(scaleTrackbar, windowName, state.scaleValue)
cv2.setTrackbarPos(typeTrackbar, windowName, state.scaleType)

while cv2.waitKey(20) != 27:
    pass

cv2.destroyAllWindows()
