"""
This example will capture a video from a webcam and store
it in a local file `output.avi`.
"""

import cv2

cap = cv2.VideoCapture(0)
if not cap.isOpened():
    raise "Error"

width = int(cap.get(cv2.CAP_PROP_FRAME_WIDTH))
height = int(cap.get(cv2.CAP_PROP_FRAME_HEIGHT))

out = cv2.VideoWriter(
        'output.avi',
        cv2.VideoWriter_fourcc('M', 'J', 'P', 'G'),
        25,
        (width, height))

cv2.namedWindow("main")

while cap.isOpened():
    if cv2.waitKey(1) & 0xFF == ord('q'):
        break

    _ret, frame = cap.read()
    out.write(frame)
    cv2.imshow('main', frame)

cap.release()
out.release()
cv2.destroyAllWindows()
