"""
Extracts a rectangular region of interest out of an image.
"""

import cv2

origin = None

def onMouseEvent(event, x, y, flags, state):
    global canvas, fresh_canvas, origin, image

    if event == cv2.EVENT_LBUTTONDOWN:
        origin = (x, y)
        canvas = fresh_canvas.copy()
    elif event == cv2.EVENT_LBUTTONUP:
        cv2.rectangle(canvas, origin, (x, y), (255, 255, 0), 2, cv2.LINE_AA)
        saveRegion(image, origin, (x, y), 'face.png')
        origin = None
    elif origin:
        canvas = fresh_canvas.copy()
        cv2.rectangle(canvas, origin, (x, y), (255, 255, 0), 2, cv2.LINE_AA)

def saveRegion(image, p1, p2, path):
    x1 = min(p1[0], p2[0])
    x2 = max(p1[0], p2[0])
    y1 = min(p1[1], p2[1])
    y2 = max(p1[1], p2[1])
    roi = image[y1:y2, x1:x2]
    cv2.imwrite(path, roi)
    print "Region saved as {}".format(path)

cv2.namedWindow("main")
cv2.setMouseCallback("main", onMouseEvent, None)

image = cv2.imread('sample.jpg', cv2.IMREAD_COLOR)
fresh_canvas = image.copy()
cv2.putText(
        fresh_canvas,
        'Choose top left corner and drag. ESC to exit' ,
        (10,30),
        cv2.FONT_HERSHEY_SIMPLEX,
        0.7,
        (255,255,255),
        2)
canvas = fresh_canvas.copy() 

while True:
    cv2.imshow('main', canvas)
    if cv2.waitKey(10) & 0xFF == 27:
        break

cv2.destroyAllWindows()
