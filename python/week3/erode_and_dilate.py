#
# Implement dilate() and erode(), and display each step
# of the algorithm using VideoWriter.
#

import cv2
import numpy as np
import matplotlib.pyplot as plt

import matplotlib
matplotlib.rcParams['figure.figsize'] = (6.0, 6.0)
matplotlib.rcParams['image.cmap'] = 'gray'

def show(im, title):
    print(title)
    print(im)
    plt.figure(title)
    plt.title(title)
    plt.imshow(im)

def dilate_and_erode(im, kernel_shape, mask, neighborhood_map, iter_callback):
    border_h, rem_h = divmod(kernel_shape[0], 2)
    border_w, rem_w = divmod(kernel_shape[1], 2)
    height, width = im.shape[:2]

    padded_image = cv2.copyMakeBorder(im, border_h, border_h, border_w, border_w, cv2.BORDER_CONSTANT, value = 0)
    transformed_image = padded_image.copy()

    unpad_image = lambda image: image[border_h:border_h+height, border_w:border_w+width]

    for h_i in range(border_h, height + border_h):
        for w_i in range(border_w, width + border_w):
            neighborhood = padded_image[(h_i-border_h) : (h_i+border_h+rem_h), (w_i-border_w) : (w_i+border_w+rem_w)][mask]
            transformed_image[(h_i, w_i)] = neighborhood_map(neighborhood)
            if iter_callback != None:
                iter_callback(unpad_image(transformed_image))
    return unpad_image(transformed_image)

def dilate(im, kernel, iter_callback = None):
    return dilate_and_erode(im, kernel.shape, kernel == 1, max, iter_callback)

def erode(im, kernel, iter_callback = None):
    return dilate_and_erode(im, kernel.shape, kernel == 1, min, iter_callback)

class MyVideoWriter:
    def __init__(self, filename, frame_size):
        self.frame_size = frame_size
        self.video_writer = cv2.VideoWriter(
            filename,
            cv2.VideoWriter_fourcc('M', 'J', 'P', 'G'),
            10,
            frame_size)

    def write_frame(self, image):
        scaledImage = cv2.resize(
                image,
                self.frame_size,
                interpolation = cv2.INTER_NEAREST)
        bgrImage = cv2.cvtColor(scaledImage * 255, cv2.COLOR_GRAY2BGR)
        self.video_writer.write(bgrImage)

    def close(self):
        self.video_writer.release()

def with_video_writer(filename, frame_size):
    video_writer = MyVideoWriter(filename, frame_size)
    yield video_writer
    video_writer.close()

def main():
    # Make a black 10x10 picture 
    im = np.zeros((10,10),dtype='uint8')
    show(im, "black image")

    # Add some white blobs to it 
    im[0,1] = 1
    im[-1,0]= 1
    im[-2,-1]=1
    im[2,2] = 1
    im[5:8,5:8] = 1
    show(im, "Original image")

    # Use a 3x3 ellipse as structuring element
    element = cv2.getStructuringElement(cv2.MORPH_ELLIPSE, (3,3))

    dialatedImage = cv2.dilate(im, element)
    show(dialatedImage, "Dilated image using OpenCV")

    for video_writer in with_video_writer('dilationScratch.avi', (50, 50)):
        video_writer.write_frame(im)
        dilated_image = dilate(im, element, video_writer.write_frame)
        show(dilated_image, "Dilated image using own implementation")

    erodedImage = cv2.erode(im, element)
    show(erodedImage, "Eroded image using OpenCV")

    for video_writer in with_video_writer('erosionScratch.avi', (50, 50)):
        video_writer.write_frame(im)
        eroded_image = erode(im, element, video_writer.write_frame)
        show(eroded_image, "Eroded image using own implementation")

    plt.show()

main()
