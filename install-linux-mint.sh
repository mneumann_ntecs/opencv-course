cvVersion="4.1.0"
cwd=$(pwd)

step1()
{
    rm -rf opencv/build
    rm -rf opencv_contrib/build
    rm -rf installation
    mkdir installation
    mkdir installation/OpenCV-"$cvVersion"
    sudo apt update -y
    sudo apt upgrade -y
}

step2()
{
    sudo apt remove -y x264 libx264-dev

    # This is missing in the install instructions
    sudo apt install -y ipython

    ## Install dependencies
    sudo apt install -y build-essential checkinstall cmake pkg-config yasm
    sudo apt install -y git gfortran
    sudo apt install -y libjpeg8-dev libpng-dev

    sudo apt install -y software-properties-common
    sudo add-apt-repository "deb http://security.ubuntu.com/ubuntu xenial-security main"
    sudo apt update -y

    sudo apt install -y libjasper1
    sudo apt install -y libtiff-dev

    sudo apt install -y libavcodec-dev libavformat-dev libswscale-dev libdc1394-22-dev
    sudo apt install -y libxine2-dev libv4l-dev
    cd /usr/include/linux
    sudo ln -s -f ../libv4l1-videodev.h videodev.h
    cd "$cwd"

    sudo apt install -y libgstreamer1.0-dev libgstreamer-plugins-base1.0-dev
    sudo apt install -y libgtk2.0-dev libtbb-dev qt5-default
    sudo apt install -y libatlas-base-dev
    sudo apt install -y libfaac-dev libmp3lame-dev libtheora-dev
    sudo apt install -y libvorbis-dev libxvidcore-dev
    sudo apt install -y libopencore-amrnb-dev libopencore-amrwb-dev
    sudo apt install -y libavresample-dev
    sudo apt install -y x264 v4l-utils

    # Optional dependencies
    sudo apt install -y libprotobuf-dev protobuf-compiler
    sudo apt install -y libgoogle-glog-dev libgflags-dev
    sudo apt install -y libgphoto2-dev libeigen3-dev libhdf5-dev doxygen
}

step3()
{
    sudo apt install -y python3-dev python3-pip python3-venv
    sudo -H pip3 install -U pip numpy
    sudo apt install -y python3-testresources

    # create virtual environment
    python3 -m venv OpenCV-"$cvVersion"-py3
    echo "# Virtual Environment Wrapper" >> ~/.bashrc
    echo "alias workoncv-$cvVersion=\"source $cwd/OpenCV-$cvVersion-py3/bin/activate\"" >> ~/.bashrc

    export VIRTUAL_ENV="/data/Projects/public/opencv-course/OpenCV-$cvVersion-py3" 
    unset PYTHONHOME
    PATH="$VIRTUAL_ENV/bin:$PATH" pip install wheel numpy scipy matplotlib scikit-image scikit-learn ipython
}

step4()
{
    git clone https://github.com/opencv/opencv.git
    cd opencv
    git checkout tags/"$cvVersion"
    cd ..

    git clone https://github.com/opencv/opencv_contrib.git
    cd opencv_contrib
    git checkout tags/"$cvVersion"
    cd ..
}

step5()
{
    cd opencv
    mkdir build
    cd build
    cmake -D CMAKE_BUILD_TYPE=RELEASE \
          -D CMAKE_INSTALL_PREFIX=$cwd/installation/OpenCV-"$cvVersion" \
          -D INSTALL_C_EXAMPLES=ON \
          -D INSTALL_PYTHON_EXAMPLES=ON \
          -D WITH_TBB=ON \
          -D WITH_V4L=ON \
          -D WITH_QT=ON \
          -D WITH_OPENGL=ON \
          -D OPENCV_PYTHON3_INSTALL_PATH=$cwd/OpenCV-$cvVersion-py3/lib/python3.6/site-packages \
          -D OPENCV_EXTRA_MODULES_PATH=../../opencv_contrib/modules \
          -D BUILD_EXAMPLES=ON ..
    make -j4
    make install
}


step1 && step2 && step3 && step4 && step5
